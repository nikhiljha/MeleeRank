from telegram.ext import Updater, CommandHandler

import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# Get token for Telegram Bot.
f = open("token","r");
token = f.read();
f.close();

# Panda best animal?
import pandas as pd;

# For now we can calculate the SR at every run. Suboptimal.
games = pd.DataFrame.from_csv('season.csv', index_col=None).rename(index=str, columns={"Timestamp": "timestamp", "Who won?": "winner", "Who lost?": "loser", "What characters? [Winner]": "loserchar", "What characters? [Loser]": "winnerchar", "What stage?": "stage"});
players = pd.DataFrame.from_csv('players.csv', index_col="name");

# Glicko2 best SR calculator?
from glicko2 import Glicko2, WIN, DRAW, LOSS;
env = Glicko2(tau=0.5);


# wget https://docs.google.com/spreadsheets/d/1AJnb5XS2c9vbCqHfcE3Bxta6XR9AC2cQSUVPr_kF_7w/export\?format\=csv\&id\=1AJnb5XS2c9vbCqHfcE3Bxta6XR9AC2cQSUVPr_kF_7w\&gid\=398626971 -O season1.csv
def calc():
    games = pd.DataFrame.from_csv('season.csv', index_col=None).rename(index=str, columns={"Timestamp": "timestamp", "Who won?": "winner", "Who lost?": "loser", "What characters? [Winner]": "loserchar", "What characters? [Loser]": "winnerchar", "What stage?": "stage"});
    elol = players[['name', 'mu', 'phi', 'sigma']].drop_duplicates().reset_index(drop=True).set_index('name');
    for index, row in games.iterrows():
        wlo = elol.loc[row["winner"]];
        llo = elol.loc[row["loser"]];
        win = env.create_rating(wlo["mu"], wlo["phi"], wlo["sigma"]);
        los = env.create_rating(llo["mu"], llo["phi"], llo["sigma"]);
        ratw = env.rate(win, [(WIN, los)]);
        ratl = env.rate(los, [(LOSS, win)]);
        elol.set_value(row["winner"], "mu", ratw.mu);
        elol.set_value(row["winner"], "phi", ratw.phi);
        elol.set_value(row["winner"], "sigma", ratw.sigma);
        elol.set_value(row["loser"], "mu", ratl.mu);
        elol.set_value(row["loser"], "phi", ratl.phi);
        elol.set_value(row["loser"], "sigma", ratl.sigma);
        return elol;

def start(bot, update):
    update.message.reply_text('Welcome to the MeleeRank bot for PHS!')

def alltiers(bot, update):
    v = calc()[['mu', 'phi']].sort_values(by='mu', ascending=False).to_string();
    update.message.reply_text(v);

def stat(bot, update, args):
    games = pd.DataFrame.from_csv('season.csv', index_col=None).rename(index=str, columns={"Timestamp": "timestamp", "Who won?": "winner", "Who lost?": "loser", "What characters? [Winner]": "loserchar", "What characters? [Loser]": "winnerchar", "What stage?": "stage"});
    r = [];
    r.append(args[0] + "'s stats:");
    r.append("Games won: " + str(len(games[games.winner == args[0]])))
    r.append("Games won with main: " + str(len(games[(games.winner == args[0]) & (games.winnerchar == players[args[0]]["main"])])))
    r.append("Games lost: " + str(len(games[games.loser == args[0]])))
    r.append("Games lost with main : " + str(len(games[(games.loser == args[0]) & (games.loserchar == players[args[0]]["main"])])))
    # Total games, total games with main.
    totg = len(games[games.loser == args[0]]) + len(games[games.winner == args[0]]);
    totm = len(games[(games.loser == args[0]) & (games.loserchar == players[args[0]]["main"])]) + len(games[(games.winner == args[0]) & (games.winnerchar == players[args[0]]["main"])]);
    r.append("Win %: " + str(100 * float(len(games[games.winner == args[0]])) / totg));
    r.append("Win % w/ main: " + str(100 * float(len(games[(games.winner == args[0]) & (games.winnerchar == players[args[0]]["main"])])) / totm))
    update.message.reply_text("\n".join(r));

updater = Updater(token)

updater.dispatcher.add_handler(CommandHandler('start', start))
updater.dispatcher.add_handler(CommandHandler('alltiers', alltiers))
updater.dispatcher.add_handler(CommandHandler('stat', stat, pass_args=True))

updater.start_polling()
updater.idle()
