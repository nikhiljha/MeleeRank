# MeleeRank

A ranking system for Super Smash Brothers Melee created for University or High School Smash clubs.

Note: The included telegram bot is extremely inefficient and should not 
be run unless you trust your players to not spam it and destroy your 
server.

## Table of Contents

*don't have one yet, please hold*

## Installation

1. Clone this repository or download the files.
2. Clone the [glicko2](https://github.com/sublee/glicko2.git) library.
3. Do `python setup.py install` in the glicko2 library, or install some other way.
4. You can now run the app with `python start.py <gamelog location>`

## Usage

1. Make a google form to collect winner, loser, winner character, loser character (use a matrix for characters), stage of each match.
2. Fill out the google form when you want to collect results.
3. Download the CSV at the end of your season, and set the headers to `timestamp,winner,loser,winnerchar,loserchar,stage`. Order does not matter.
4. `python start.py my_season_csv_file.csv`
5. Either `>` the output to a text file or just copy and paste the output to share it.
6. Add your players to `players.csv` in the format `name,main,mu,phi,sigma`.

*note: good values for mu (skill rating) phi (confidence) and sigma (volatility) are 1500, 350 and 0.06 (respectively), though any value can be used*
