# Debug Info
print("MeleeRank v0.1.3");
print("by Nikhil Jha");

# Panda best animal?
import pandas as pd;
idx = pd.IndexSlice;

# Glicko2 best SR calculator?
from glicko2 import Glicko2, WIN, DRAW, LOSS;
env = Glicko2(tau=0.5);

# CLI Arguments best config option?
import sys;
gamelist = sys.argv[1];

# Import the data.
print("Importing game data from " + gamelist);
games = pd.DataFrame.from_csv(gamelist, index_col=None);
print("Importing player data from players.csv...")
players = pd.DataFrame.from_csv('players.csv', index_col=None);

# Print data.
print("This is some sample data I got from the file...");
print(games.head());
print("");

# How many games were played?
print("Alright, let's get started!");
print("There were " + str(len(games)) + " games played over the course of the season.");
print("");

# Who were the biggest winners and losers?
print("The biggest winner was " + str(games.winner.value_counts().keys()[0]) + " with " + str(games.winner.value_counts().max()) + " wins.");
print("The biggest loser was " + str(games.loser.value_counts().keys()[0]) + " with " + str(games.loser.value_counts().max()) + " losses.");
print("");

print("Okay, now it's time for win/loss by main.");
print("");

# What happened with each individual player?
for index, row in players.iterrows():
    print(row["name"] + "'s stats:");
    print("Games won: " + str(len(games[games.winner == row["name"]])))
    print("Games won with main: " + str(len(games[(games.winner == row["name"]) & (games.winnerchar == row["main"])])))
    print("Games lost: " + str(len(games[games.loser == row["name"]])))
    print("Games lost with main : " + str(len(games[(games.loser == row["name"]) & (games.loserchar == row["main"])])))
    # Total games, total games with main.
    totg = len(games[games.loser == row["name"]]) + len(games[games.winner == row["name"]]);
    totm = len(games[(games.loser == row["name"]) & (games.loserchar == row["main"])]) + len(games[(games.winner == row["name"]) & (games.winnerchar == row["main"])]);
    print("Win %: " + str(100 * float(len(games[games.winner == row["name"]])) / totg));
    print("Win % w/ main: " + str(100 * float(len(games[(games.winner == row["name"]) & (games.winnerchar == row["main"])])) / totm))
    print("");

print("Whew, that was a lot.");
print("Time for win/loss by player. Who does everyone consistently win/lose to?");
for index, row in players.iterrows():
	print(row["name"] + "'s stats: w/l");
	for i, r in players.iterrows():
		print(r["name"] + ": " + str(len(games[(games.winner == row["name"]) & (games.loser == r["name"])])) + "/" + str(len(games[(games.winner == r["name"]) & (games.loser == row["name"])])));
	print("");

print("Finally, it's time to calculate ELO for the final ranking. Here are the initial seeds.");
for index, row in players.iterrows():
	print(row["name"] + ": " + str(row["mu"]));

print("");
print("Calculating ELOs...")

elol = players.set_index('name');
for index, row in games.iterrows():
	wlo = elol.loc[row["winner"]];
	llo = elol.loc[row["loser"]];
	win = env.create_rating(wlo["mu"], wlo["phi"], wlo["sigma"]);
	los = env.create_rating(llo["mu"], llo["phi"], llo["sigma"]);
	ratw = env.rate(win, [(WIN, los)]);
	ratl = env.rate(los, [(LOSS, win)]);
	elol.set_value(row["winner"], "mu", ratw.mu);
	elol.set_value(row["winner"], "phi", ratw.phi);
	elol.set_value(row["winner"], "sigma", ratw.sigma);
	elol.set_value(row["loser"], "mu", ratl.mu);
	elol.set_value(row["loser"], "phi", ratl.phi);
	elol.set_value(row["loser"], "sigma", ratl.sigma);

with pd.option_context('display.max_rows', None, 'display.max_columns', 3):
    print(elol[['mu', 'phi']].sort_values(by='mu', ascending=False));

elol.to_csv("player_neo.csv");